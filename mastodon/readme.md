# Mastodon Modern
Makes Mastodon look a bit nicer :)

| ![](https://codeberg.org/Freeplay/UserStyles/raw/branch/main/mastodon/images/mastodon-modern.png) | ![](https://codeberg.org/Freeplay/UserStyles/raw/branch/main/mastodon/images/mastodon-modern-multicolumn.png) | ![](https://codeberg.org/Freeplay/UserStyles/raw/branch/main/mastodon/images/mastodon-modern-post.png) | ![](https://codeberg.org/Freeplay/UserStyles/raw/branch/main/mastodon/images/mastodon-modern-post-dark.png) |
| --- | --- | --- | --- |

### **> [Install for any server using the Stylus browser extension](https://userstyles.world/style/4773) <**
#### **WARNING:** Glitch-soc & its derivatives are not supported.


## Example for adding to your own instance:

https://github.com/Coffeedon/Cofffee-Assets#to-use-a-pre-made-theme

- **Make sure to use the compiled code from [this repo's `modern.css`](https://codeberg.org/Freeplay/UserStyles/src/branch/main/mastodon/modern.css)**
- **For light theme, you may need to include `@import 'mastodon-light/diff';`**
- **Same goes for the High Contrast theme `@import 'contrast/diff';`**
- While not tested thoroughly, could work well with other color themes, as well. 

**Note:** The UserStyle usually receives updates earlier to make sure everything is ready for the compiled CSS. 

## Attribution
If you use this style for your own server, please make sure to at least credit me somewhere with a [link to my website](https://freeplay.codeberg.page/) or [to this style's repository](https://codeberg.org/Freeplay/UserStyles/src/branch/main/mastodon) :)

Make sure to see the license below for more details:

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>. 